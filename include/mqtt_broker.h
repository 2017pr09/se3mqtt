#ifndef MQTT_BROKER_H
  #define MQTT_BROKER_H

  // broker parameters
  #define mqtt_topic "secube_test"
  #define DEFAULT_HOST "localhost"
  #define DEFAULT_PORT 1883
  #define DEFAULT_KEEPALIVE 60
  #define DEFAULT_QOS 0

  // system status
  #define STATUS_ERROR     -1
  #define STATUS_OK         0

  // libraries
  #include <iostream>
  #include <string>
  #include "config.h"

  #include <mosquitto.h>

  using namespace std;

/******************************************************************************/
/************************************BROKER************************************/
/******************************************************************************/

  class mqtt_broker {
      int broker_id           = -1;
      bool init_success       = false;
      bool is_connected       = false;
      bool is_run             = false;
      string broker_name      = string("");
      string broker_host      = string(DEFAULT_HOST);
      int broker_port         = DEFAULT_PORT;
      int broker_keepalive    = DEFAULT_KEEPALIVE;
      int broker_qos          = DEFAULT_QOS;
      struct mosquitto *mosq  = NULL;
    public:
      mqtt_broker(int l_broker_id, string l_broker_name, string l_broker_host, int l_broker_port);
      ~mqtt_broker(void);
      bool      broker_init(void);
      bool      broker_run(void);
      bool      broker_stop(void);
      int       broker_connect(void);
      int       broker_disconnect(void);
      int       broker_subscribe(string l_topic);
      int       broker_unsubscribe(string l_topic);
      int       broker_publish(uint8_t* l_payload, int l_payload_len, string l_topic);
      void      broker_set_message_callback(void (*on_message)(struct mosquitto *, void *, const struct mosquitto_message *));
  };



#endif
