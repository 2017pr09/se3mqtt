#include "../include/main.h"

static int run = 1;

using namespace std;

void handle_signal(int s)
{
	run = 0;
}

void new_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	bool match = 0;
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "NEW: MQTT got message for topic " << string(message->topic) << endl;
  #endif

	mosquitto_topic_matches_sub(mqtt_topic, message->topic, &match);
	if (match) {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "NEW: MQTT got message " <<  string((char*) message->payload) << " for topic " << string(message->topic) << endl;
    #endif
	}

}

int main(int argc, char *argv[])
{
	signal(SIGINT, handle_signal);
	signal(SIGTERM, handle_signal);

	int broker_id = 0;
	string broker_name("test_broker");
	string broker_host("localhost");
	int broker_port = 1883;

	string broker_topic("secube_test");

	mqtt_broker *local_broker = NULL;

	local_broker = new mqtt_broker(broker_id, broker_name, broker_host, broker_port);

	local_broker->broker_init();
	local_broker->broker_set_message_callback(new_message_callback);

	local_broker->broker_connect();
	local_broker->broker_subscribe(broker_topic);

	local_broker->broker_run();

	while(run){
		//local_broker->broker_publish(broker_topic);
		sleep(1);
	}

	local_broker->broker_stop();

	delete(local_broker);

	return 0;
}
