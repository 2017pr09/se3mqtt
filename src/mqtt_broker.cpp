#include "../include/mqtt_broker.h"

using namespace std;

/******************************************************************************/
/******************************MQTT CALLBACKS**********************************/
/******************************************************************************/
void connect_callback(struct mosquitto *mosq, void *obj, int result)
{
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT connect callback with result " <<  result << endl;
  #endif
}

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	bool match = 0;
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT got message for topic " << string(message->topic) << endl;
  #endif

	mosquitto_topic_matches_sub(mqtt_topic, message->topic, &match);
	if (match) {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT got message " <<  string((char*) message->payload) << " for topic " << string(message->topic) << endl;
    #endif
	}

}

/******************************************************************************/
/******************************BROKER METHODS**********************************/
/******************************************************************************/

mqtt_broker::mqtt_broker(int l_broker_id, string l_broker_name, string l_broker_host, int l_broker_port){
  broker_id = l_broker_id;
  broker_name = l_broker_name;
  broker_host = l_broker_host;
  broker_port = l_broker_port;
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: New MQTT broker: " << broker_name << " - ID: " << broker_id << " - Host : " << broker_host << " - PORT: " << broker_port << endl;
  #endif
}

mqtt_broker::~mqtt_broker(void){
  if(mosq){
    if(is_connected){
      broker_disconnect();
    }
    if(is_run){
      broker_stop();
    }
    mosquitto_destroy(mosq);
  }
  mosquitto_lib_cleanup();
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: Delete MQTT broker " << broker_name << endl;
  #endif
}

bool mqtt_broker::broker_init(void){
  bool ret_value = true;

  mosquitto_lib_init();
  if(!init_success){
    mosq = mosquitto_new(broker_name.c_str(), true, 0);
    if(mosq){
      mosquitto_connect_callback_set(mosq, connect_callback);
  		mosquitto_message_callback_set(mosq, message_callback);
      init_success = true;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker init" << endl;
      #endif
    } else {
      ret_value = false;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker init FAILED" << endl;
      #endif
    }
  } else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker already init" << endl;
    #endif
  }

  return ret_value;
}

bool mqtt_broker::broker_run(void){
  bool ret_value = true;

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT broker run" << endl;
  #endif

  if(init_success && is_connected){
    if(!is_run){
      if(mosquitto_loop_start(mosq) != MOSQ_ERR_SUCCESS){
        ret_value = false;
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: MQTT broker run FAILED" << endl;
        #endif
      }
    } else {
      ret_value = false;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker already running" << endl;
      #endif
    }
  } else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker run FAILED" << endl;
    #endif
  }

  return ret_value;
}

bool mqtt_broker::broker_stop(void){
  bool ret_value = true;

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT broker stop" << endl;
  #endif

  if(init_success && !is_connected){
    if(is_run){
      if(mosquitto_loop_stop(mosq, true) != MOSQ_ERR_SUCCESS){
        ret_value = false;
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: MQTT broker stop FAILED" << endl;
        #endif
      }
    } else {
      ret_value = false;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker is not running" << endl;
      #endif
    }
  } else {
    ret_value = false;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker stop FAILED" << endl;
    #endif
  }

  return ret_value;
}


int mqtt_broker::broker_connect(void){
  int ret_value = STATUS_OK;
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT broker connect" << endl;
  #endif

  if(init_success){
    if((mosquitto_connect(mosq, broker_host.c_str(), broker_port, broker_keepalive)) != MOSQ_ERR_SUCCESS){
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker connect FAILED" << endl;
      #endif
    } else {
      is_connected = true;
    }
  } else {
    ret_value = STATUS_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker connect FAILED" << endl;
    #endif
  }
  return ret_value;
}

int mqtt_broker::broker_disconnect(void){
  int ret_value = STATUS_OK;
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT broker disconnect" << endl;
  #endif

  if(init_success && is_run){
    if((mosquitto_disconnect(mosq)) != MOSQ_ERR_SUCCESS){
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker disconnect FAILED" << endl;
      #endif
    } else {
      is_connected = false;
    }
  } else {
    ret_value = STATUS_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker disconnect FAILED" << endl;
    #endif
  }
  return ret_value;
}

int mqtt_broker::broker_subscribe(string l_topic){
  int ret_value = STATUS_OK;

  if(init_success && is_connected){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker subscribe to topic " << l_topic << endl;
    #endif
    if((mosquitto_subscribe(mosq, NULL, l_topic.c_str(), broker_qos)) != MOSQ_ERR_SUCCESS){
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker subscribe to topic " << l_topic << " FAILED!"<< endl;
      #endif
    }
  } else {
    ret_value = STATUS_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker subscribe to topic " << l_topic << " FAILED!"<< endl;
    #endif
  }

  return ret_value;
}

int mqtt_broker::broker_unsubscribe(string l_topic){
  int ret_value = STATUS_OK;

  if(init_success && is_connected){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker unsubscribe from topic " << l_topic << endl;
    #endif
    if((mosquitto_unsubscribe(mosq, NULL, l_topic.c_str())) != MOSQ_ERR_SUCCESS){
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker unsubscribe from topic " << l_topic << "FAILED!"<< endl;
      #endif
    }
  } else {
    ret_value = STATUS_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker unsubscribe from topic " << l_topic << "FAILED!"<< endl;
    #endif
  }

  return ret_value;
}

int mqtt_broker::broker_publish(uint8_t* l_payload, int l_payload_len, string l_topic){
  int ret_value = STATUS_OK;

  if(init_success && is_connected){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker publish message " << (void *) l_payload << " to topic " << l_topic << endl;
    #endif
    if((mosquitto_publish(mosq, NULL, l_topic.c_str(), l_payload_len, (void *)l_payload, broker_qos, false)) != MOSQ_ERR_SUCCESS){
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: MQTT broker publish to topic " << l_topic << "FAILED!"<< endl;
      #endif
    }
  } else {
    ret_value = STATUS_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: MQTT broker publish to topic " << l_topic << "FAILED!"<< endl;
    #endif
  }

  return ret_value;
}


void mqtt_broker::broker_set_message_callback(void (*on_message)(struct mosquitto *, void *, const struct mosquitto_message *)){
  mosquitto_message_callback_set(mosq, on_message);
  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT new callback set" << endl;
  #endif
}
